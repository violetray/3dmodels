# 3D Models

A collection of 3D models, specifically targeted for 3D resin printing
(filament printing should work fine as well, just ignore the CHITUBOX files).

## Folders

### Blender

A collection of blender project files with modifications or fixes to models.

To print a model, export from Blender into a `.stl` (make sure to check out the
handy 'Selection only' checkbox) and then import this into your 3D slicer, such
as CHITUBOX.

### CHITUBOX

A collection of [CHITUBOX](https://www.chitubox.com/) project files. These
include pre-placed supports that should "just work". Tested on a Elagoo Mars 2
Pro.

### SFM

A collection of [Source Filmmaker](https://www.sourcefilmmaker.com/) decompiled
model files. These are made from SFM assets using the "decompile" feature of
[Crowbar](https://github.com/ZeqMacaw/Crowbar) (runs fine in
[Wine](https://appdb.winehq.org/)). These can be imported into Blender using
[Blender Source Tools](https://steamreview.org/BlenderSourceTools/).

It's possible to also import animations from Source Filmmaker into Blender in
order to pose your character model. Unfortunately I couldn't get SFM to run on
Linux, so I haven't tried this yet.

### STLs

Good ol' STL files. These can be imported into your 3D slicer software, such as
CHITUBOX, and then printed.

## Models

Some models and their explanations.

### Blender

* `ZZZ_Build Plate Template...` files
  * These are template files containing an outline of
    the exact build dimensions of a printer. The printer's name is included in
    the filename. Feel free to resize to your own printer and upload a new
    template. File units are in millimetres.
